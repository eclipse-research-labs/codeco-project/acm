# Copyright (c) 2024 Red Hat, Inc
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     [name] - [contribution]

#!/bin/bash
# Unset variables to prevent overlapping issues
unset jsonConfigPath
unset podInfo
unset nodeIPs
unset entry
unset nodeName
unset nodeIP
unset nodesArray
unset linksArray
unset linkPairs
unset srcNode
unset dstNode
unset key1
unset key2
unset configFilePath
unset pods
unset podName
unset pod
unset output_file
unset nodes
unset ip_counter
unset helm_chart_path
unset non_deployments_file
unset deployment_file
unset deployment_count
unset doc
unset in_deployment
unset file_name
cd ../secure-connectivity
# Filename for the JSON config
jsonConfigPath="./configs/switchConfig.json"

# Use kubectl to get pods, grep for those starting with 'l2sm-switch', and parse with awk
mapfile -t podInfo < <(
  kubectl get pods -n he-codeco-netma -o wide |
  grep 'l2sm-switch' |
  awk '{for (i=1; i<=NF; i++) { 
    if ($i ~ /^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/) { 
      print $(i+1) ":" $i; 
      break; 
    } 
  }}'
)

# Associative array to store node names and their respective IPs
declare -A nodeIPs

# Fill the associative array with node names as keys and IPs as values
for entry in "${podInfo[@]}"
do
  nodeName="${entry%%:*}"
  nodeIP="${entry##*:}"
  nodeIPs["$nodeName"]="$nodeIP"
done

# Prepare Nodes array
nodesArray=()
for nodeName in "${!nodeIPs[@]}"
do
  nodesArray+=("{\"name\": \"$nodeName\", \"nodeIP\": \"${nodeIPs[$nodeName]}\"}")
done

# Prepare Links array
linksArray=()
declare -A linkPairs
for srcNode in "${!nodeIPs[@]}"
do
  for dstNode in "${!nodeIPs[@]}"
  do
    key1="$srcNode|$dstNode"
    key2="$dstNode|$srcNode"

    if [[ "$srcNode" != "$dstNode" && -z "${linkPairs[$key1]}" && -z "${linkPairs[$key2]}" ]]; then
      linksArray+=("{\"endpointA\": \"$srcNode\", \"endpointB\": \"$dstNode\"}")
      linkPairs["$key1"]=1
    fi

  done
done

# Generate JSON output
echo "{
  \"Nodes\": [
    $(IFS=,; echo "${nodesArray[*]}")
  ],
  \"Links\": [
    $(IFS=,; echo "${linksArray[*]}")
  ]
}" > "$jsonConfigPath"

echo "Network topology configuration has been generated at $jsonConfigPath."


# Unset variables to prevent overlapping issues
unset configFilePath
unset podInfo
unset pods
unset entry
unset podName
unset nodeName
unset pod

# Path to the configuration file
configFilePath="./configs/switchConfig.json"

# Use kubectl to get pods and grep for those starting with 'l2sm-switch'
# Then, use awk to extract the pod name and node name by matching an IP pattern and taking the next column as the node
mapfile -t podInfo < <(kubectl get pods -n he-codeco-netma -o wide | grep 'l2sm-switch' | awk '{for (i=1; i<=NF; i++) { if ($i ~ /^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/) { print $1 ":" $(i+1); break; } }}')

# Declare an associative array to store pod names and their respective nodes
declare -A pods

# Fill the associative array with pod names as keys and node names as values
for entry in "${podInfo[@]}"
do
  podName="${entry%%:*}"
  nodeName="${entry##*:}"
  pods["$podName"]=$nodeName
done

# Loop through the array
for pod in "${!pods[@]}"
do
  nodeName=${pods[$pod]}
  echo $pod

  # Copy the configuration file to the pod
  echo "Copying config file to $pod..."
  kubectl cp -n he-codeco-netma "$configFilePath" "$pod:/etc/l2sm/switchConfig.json"

  # Execute the script inside the pod
  echo "Executing configuration script on $pod..."
  kubectl exec -it "$pod" -n he-codeco-netma -- /bin/bash -c "l2sm-vxlans --node_name=$nodeName /etc/l2sm/switchConfig.json"
done

echo "Configuration deployment completed."


# Define the output file path
output_file="./LPM/chart/values.yaml"

# Create or overwrite the file with the initial content
cat > "$output_file" << EOF
global:
  nodes:
EOF

# Get the list of node names
nodes=($(kubectl get nodes -o jsonpath='{range .items[*]}{.metadata.name}{"\n"}{end}'))

# Initialize IP address counter starting from 2 (as per 10.0.0.(n+1))
ip_counter=2

# Loop through each node and append the required YAML content
for node in "${nodes[@]}"; do
  cat >> "$output_file" << EOF
    - name: $node
      ip: 10.0.0.$ip_counter/24
      metrics:
        rttInterval: 10
        throughputInterval: 20
        jitterInterval: 5
EOF
  ip_counter=$((ip_counter + 1))
done

# Append the network and namespace information
cat >> "$output_file" << EOF
  network:
    name: lpm-network
  namespace: he-codeco-netma
EOF


# Define file paths
helm_chart_path="./LPM/chart/"
non_deployments_file="non_deployments.yaml"

# Remove previous files if they exist
rm -f $non_deployments_file
rm -f deployments_*.yaml

# Run helm template and process the output
helm template $helm_chart_path | awk '
  BEGIN { in_deployment = 0; doc = "" }
  /^---$/ {
    if (doc != "") {
      if (in_deployment == 0) {
        print doc >> "'$non_deployments_file'"
        print "---" >> "'$non_deployments_file'"
      } else {
        # Increment deployment counter
        deployment_count += 1
        file_name = sprintf("deployments_%02d.yaml", deployment_count)
        print doc >> file_name
        print "---" >> file_name
      }
    }
    doc = ""; in_deployment = 0
  }
  {
    doc = doc $0 "\n"
    if ($1 == "kind:" && $2 == "Deployment") {
      in_deployment = 1
    }
  }
  END {
    if (doc != "") {
      if (in_deployment == 0) {
        print doc >> "'$non_deployments_file'"
      } else {
        deployment_count += 1
        file_name = sprintf("deployments_%02d.yaml", deployment_count)
        print doc >> file_name
      }
    }
  }
'

# Apply non-deployment resources
echo "Applying non-deployment resources..."
kubectl apply -f $non_deployments_file

# Apply deployments one by one with a 5-second delay
echo "Applying deployments individually with a 5-second delay..."
for deployment_file in deployments_*.yaml; do
  echo "Applying $deployment_file..."
  kubectl apply -f "$deployment_file"
  sleep 5
done

echo "Deployment process completed."
